#!/usr/bin/python3
import random
import socket
import sys

mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
mySocket.bind((sys.argv[1], int(sys.argv[2])))

mySocket.listen(5)

while True:
    print('Waiting for connections')
    (recvSocket, address) = mySocket.accept()
    print(address)
    print('HTTP request received:')
    petition = recvSocket.recv(1024).decode('utf-8')  # la respuesta viene en bytes, si quiero imprimirlo tengo que pasarlo a str
    print(petition)

    urls = ['https://www.urjc.es', 'https://www.marca.es', 'https://www.elmundo.es']
    randomUrl = random.choice(urls)

    random_numbers = [300, 301, 302, 303, 304, 307, 308]
    redirect_code = random.choice(random_numbers)

    recvSocket.send(f"HTTP/1.1 {redirect_code} Redirect\r\n".encode('utf-8') +
                    f"Redireccionando: {randomUrl}\r\n\r\n".encode('utf-8') + #para separar las cabeceras de envio de la pagina HTML usamos \r\n\r\n

                    b"<html><body>" +
                    b"<h1> Redireccionando a: " + bytes(randomUrl, 'utf-8') + b"</h1>" +
                    b"</body></html>" +
                    b"\r\n")

    recvSocket.close()